package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysRolePrivilege;

public interface SysRolePrivilegeMapper extends BaseMapper<SysRolePrivilege> {
}