package com.xxll.service.impl;

import com.xxll.service.SysRolePrivilegeUserService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxll.mapper.SysRolePrivilegeUserMapper;
import com.xxll.domain.SysRolePrivilegeUser;

@Service
public class SysRolePrivilegeUserServiceImpl extends ServiceImpl<SysRolePrivilegeUserMapper, SysRolePrivilegeUser> implements SysRolePrivilegeUserService {

}
